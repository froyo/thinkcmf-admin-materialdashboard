### thinkcmf-admin-materialdashboard

*	thinkcmf-admin-materialdashboard 已前端模板[material-dashboard](https://www.creative-tim.com/product/material-dashboard) 作为基础，结合thinkcmf官方bootstrap3 的模板修改而来。模板并不是单纯修改css样式，改动了页面html结构，所以**请勿直接覆盖项目模板**


*   thinkcmf-admin-materialdashboard 模板采用thinkphp模板继承的方式，改变官方默认模板引入公共头文件的方式，方便样式修改，以及后期改为页面形式而不是iframe的方式

*	改动php主要是admin/index/index中显示菜单的php代码

*	所有页面，如果需要在模板中定义php函数，请在phpscript block中操作或者在对应的block中，block之外的所有代码无效!!!

*	在admin/index/index中添加了一项demo菜单，用于给出表格，图表，表单页面的模板实例，记得删除

### 界面效果显示

![chart teamplate](http://froyoimg.static.aiaiaini.com/cmf_chart.png) 

![table teamplate](http://froyoimg.static.aiaiaini.com/cmf_table.png)

![form teamplate](http://froyoimg.static.aiaiaini.com/cmf_form.png)

![setting teamplate](http://froyoimg.static.aiaiaini.com/cmf_setting.png)

### 使用方式

*	下载文件，解压到public\themes\目录下
*	在配置文件中设置```cmf_admin_default_theme=>admin_materialdashboard```

*   新建user\controlles\AdminDemoController控制器,用于显示页面demo

```
namespace app\user\controller;

use cmf\controller\AdminBaseController;
use cmf\lib\Upload;
use think\View;


class AdminDemoController extends AdminBaseController
{
	public function table(){
		return $this->fetch();
	}
	public function chart(){
		return $this->fetch();
	}
	public function form(){
		return $this->fetch();
	}
}

```